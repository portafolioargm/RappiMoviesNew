**Ejercicio Técnico**

**  RappiMovies**

**  App de Películas/Series**


1. Películas y/o Series categorizadas por Popular y Top Rated. **(Aplicado)**
2. Detalle de Película y/o Serie. **(Aplicado)**
3. Buscador de Películas y/o Series por nombre.
4. Visualización de Videos en el detalle. **(Aplicado)**
5. La App debe poder funcionar offline. **(Aplicado)**
6. Pruebas Unitarias. **(Aplicado)**
7. Transiciones/Animacione **(Aplicado)**

Ejemplo

- Entre un listado y detalle.

- En listados/detalle al hacer scroll

8. API: https://developers.themoviedb.org/ **(Aplicado)**
9. Usar Kotlin. **(Aplicado)**
10. No hay restricción de bibliotecas externas.
11. Subir el proyecto a Github/Bitbucket/Gitlab o cualquier otro que utilice Git. **(Aplicado)**
12. La App debe ser escalable, con lo cual toda buena práctica que conozca aplíquela. **(Aplicado)**

  - Usar algún patrón de arquitectura: ej. MVVM, MVP, etc. **(Aplicado)**
  
  - Inyectar dependencias. Si se usa alguna biblioteca mejor. **(Aplicado)**
  
13. Detalla cómo harías todo aquello que no hayas llegado a completar en el
README.md del proyecto. Sugerencia: realizar el ejercicio durante el fin de semana.
14. Revisa a detalle que la prueba no Crashea
15. Buena navegación para el usuario **(Aplicado)**
16. Debe soportar paginado
17. Debe diferenciarse entre películas y series. Te recomendamos que agregues un diseño y presentación lo mas estructurada posible **(Aplicado)**
18. Utiliza dimens
19. Buen diseño **(Aplicado)**
20. Buen manejo de Mocks **(Aplicado)**
21. Buen manejo de onCreate **(Aplicado)**
22. Te recomendamos agregar categorías e información de las series y peliculas


**Recomendaciones para Codigo: Código**

**  Positivos**

  - Uso de arquitectura MVVM **(Aplicado)**

  - Uso de Rx

  - Uso de Dagger **(Aplicado)**

  - Uso de ViewBinding **(Aplicado)**

  - Uso de Navigation **(Aplicado)**

  - Uso de LiveData **(Aplicado)**

  - Uso de Room **(Aplicado)**

  - Uso de ViewModelProvider **(Aplicado)**

  - Scope functions **(Aplicado)**

  - Hace null la instancia de viewbinding Entregables
