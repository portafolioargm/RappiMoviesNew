package com.argm.rappimoviesnew.data.model

data class VideosModel (
    val id : Int?,
    val results : List<VideoModel>
)