package com.argm.rappimoviesnew.data.database.dao

import androidx.annotation.NonNull
import androidx.room.*
import com.argm.rappimoviesnew.data.database.entities.MovieEntity
import com.argm.rappimoviesnew.data.database.entities.TvEntity
import com.argm.rappimoviesnew.data.database.entities.VideoEntity

@Dao
interface VideoDao {

    @Query("SELECT * FROM video_table WHERE id_movie= :idMovie")
    suspend fun getAllVideosDB(idMovie: Int): List<VideoEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertVideosOnlyTypeDB(videos: List<VideoEntity>)

    @Query("DELETE FROM video_table WHERE id_movie= :idMovie")
    suspend fun deleteVideosMovieDB(idMovie: Int?)

}