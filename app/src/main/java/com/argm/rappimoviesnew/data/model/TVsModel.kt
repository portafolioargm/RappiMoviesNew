package com.argm.rappimoviesnew.data.model

data class TVsModel (
    val page : Int?,
    val results : List<TvModel>
)