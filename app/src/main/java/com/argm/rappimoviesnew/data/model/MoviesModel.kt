package com.argm.rappimoviesnew.data.model

data class MoviesModel (
    val page : Int?,
    val results : List<MovieModel>
)