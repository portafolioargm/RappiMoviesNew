package com.argm.rappimoviesnew.data.model

import android.graphics.Bitmap
import android.os.Parcelable
import android.provider.ContactsContract
import kotlinx.parcelize.Parcelize

@Parcelize
data class TvModel(
    val poster_path: String?,
    val popularity:Float?,
    val id: Int?,
    val backdrop_path: String?,
    val vote_average:Float?,
    val overview:String?,
    val first_air_date:String?,
    val genre_ids:ArrayList<Int>?,
    val original_language:String?,
    val vote_count:Int?,
    val name: String?,
    val original_name:String?
):Parcelable