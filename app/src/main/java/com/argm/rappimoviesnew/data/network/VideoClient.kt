package com.argm.rappimoviesnew.data.network

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.model.VideosModel
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface VideoClient {
    @GET
    suspend fun getVideosForId(@Url url: String?): Response<VideosModel>
}