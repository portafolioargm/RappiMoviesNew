package com.argm.rappimoviesnew.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.argm.rappimoviesnew.domain.model.Tv

@Entity(tableName = "tv_table")
data class TvEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="indice") val indice: Int = 0,
    @ColumnInfo(name="poster_path") val poster_path: String?,
    @ColumnInfo(name="popularity") val popularity:Float?,
    @ColumnInfo(name="id") val id: Int?,
    @ColumnInfo(name="backdrop_path") val backdrop_path: String?,
    @ColumnInfo(name="vote_average") val vote_average:Float?,
    @ColumnInfo(name="overview") val overview:String?,
    @ColumnInfo(name="first_air_date") val first_air_date:String?,
    @ColumnInfo(name="original_language") val original_language:String?,
    @ColumnInfo(name="vote_count") val vote_count:Int?,
    @ColumnInfo(name="name") val name: String?,
    @ColumnInfo(name="original_name") val original_name:String?,
    @ColumnInfo(name="type") val type:Int
)

fun Tv.toDataBase(type: Int) = TvEntity(
    vote_count=vote_count,
    id=id,
    vote_average=vote_average,
    name=name,
    popularity=popularity,
    poster_path=poster_path,
    original_language=original_language,
    original_name=original_name,
    backdrop_path=backdrop_path,
    overview=overview,
    first_air_date=first_air_date,
    type=type,
)