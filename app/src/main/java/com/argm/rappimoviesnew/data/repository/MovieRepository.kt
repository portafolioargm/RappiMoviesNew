package com.argm.rappimoviesnew.data.repository

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.database.dao.MovieDao
import com.argm.rappimoviesnew.data.database.entities.MovieEntity
import com.argm.rappimoviesnew.data.network.ApiService
import com.argm.rappimoviesnew.domain.model.Movie
import com.argm.rappimoviesnew.domain.model.toDomain
import javax.inject.Inject

class MovieRepository  @Inject constructor(
    private val api : ApiService,
    private val dao : MovieDao
    ){
    suspend fun getMoviesFromApi(type_movies: Int?): List<Movie> {
        if (type_movies==Constants.TYPE_MOVIES_POPULAR){
            return try {
                (api.getMoviesPopular()).results.map { it.toDomain(type_movies) }
            }catch (e: Exception){
                emptyList()
            }

        }else if (type_movies==Constants.TYPE_MOVIES_TOP_RATED){
            return try {
                (api.getMoviesTopRated()).results.map { it.toDomain(type_movies) }
            }catch (e: Exception){
                emptyList()
            }
        }else{
            return try {
                (api.getMoviesPopular()).results.map { it.toDomain(type_movies!!) }
            }catch (e: Exception){
                emptyList()
            }
        }
    }

    suspend fun getMoviesFromDB(type_movies: Int): List<Movie> {
        return try {
            (dao.getAllMoviesDB(type_movies)).map { it.toDomain(type_movies!!) }
        }catch (e: Exception){
            emptyList()
        }
    }

    suspend fun insertMoviesDB(movies: List<MovieEntity>){
        dao.insertMoviesOnlyTypeDB(movies)
    }

    suspend fun removeMoviesDB(movies: List<MovieEntity>){
        dao.deleteMoviesOnlyTypeDB(movies)
    }

    suspend fun removeForTypeMoviesDB(type: Int){
        dao.deleteMoviesForTypeDB(type)
    }
}