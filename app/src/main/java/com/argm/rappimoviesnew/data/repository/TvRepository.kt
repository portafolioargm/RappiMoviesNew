package com.argm.rappimoviesnew.data.repository

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.database.dao.TvDao
import com.argm.rappimoviesnew.data.database.entities.TvEntity
import com.argm.rappimoviesnew.data.network.ApiService
import com.argm.rappimoviesnew.domain.model.Tv
import com.argm.rappimoviesnew.domain.model.toDomain
import javax.inject.Inject

class TvRepository  @Inject constructor(
    private val api : ApiService,
    private val dao : TvDao
    ){
    suspend fun getTVsFromApi(type_tv: Int?): List<Tv> {
        if (type_tv==Constants.TYPE_MOVIES_POPULAR){
            return try {
                (api.getTVsPopular()).results.map { it.toDomain(type_tv) }
            }catch (e: Exception){
                emptyList()
            }
        }else if (type_tv==Constants.TYPE_MOVIES_TOP_RATED){
            return try {
                (api.getTVsTopRated()).results.map { it.toDomain(type_tv) }
            }catch (e: Exception){
                emptyList()
            }
        }else{
            return try {
                (api.getTVsPopular()).results.map { it.toDomain(type_tv!!) }
            }catch (e: Exception){
                emptyList()
            }
        }
    }
    suspend fun getTvsFromDB(type_tv: Int): List<Tv> {
        return try {
            (dao.getAllTvsDB(type_tv)).map { it.toDomain(type_tv!!)}
        }catch (e: Exception){
            emptyList()
        }
    }

    suspend fun insertTvsDB(tvs: List<TvEntity>){
        dao.insertTvsOnlyTypeDB(tvs)
    }

    suspend fun removeTvsDB(tvs: List<TvEntity>){
        dao.deleteTvsOnlyTypeDB(tvs)
    }

    suspend fun removeForTypeTvsDB(type_tv: Int){
        dao.deleteMoviesForTypeDB(type_tv)
    }
}