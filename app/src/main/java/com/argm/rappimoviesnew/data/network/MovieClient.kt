package com.argm.rappimoviesnew.data.network

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.model.MoviesModel
import retrofit2.Response
import retrofit2.http.*

interface MovieClient {
    @GET(Constants.URL_MOVIES_POPULAR)
    suspend fun getMoviesPopular(): Response<MoviesModel>
    @GET(Constants.URL_MOVIES_TOP_RATED)
    suspend fun getMoviesTopRated(): Response<MoviesModel>
}