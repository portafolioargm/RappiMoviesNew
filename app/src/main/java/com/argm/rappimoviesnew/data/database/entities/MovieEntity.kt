package com.argm.rappimoviesnew.data.database.entities

import android.graphics.Bitmap
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.argm.rappimoviesnew.domain.model.Movie

@Entity(tableName = "movie_table")
data class MovieEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="indice") val indice: Int = 0,
    @ColumnInfo(name="vote_count") val vote_count: Int?,
    @ColumnInfo(name="id") val id: Int?,
    @ColumnInfo(name="video") val video: Boolean?,
    @ColumnInfo(name="vote_average") val vote_average: Float?,
    @ColumnInfo(name="title") val title:String?,
    @ColumnInfo(name="name") val name: String?,
    @ColumnInfo(name="popularity") val popularity:Float?,
    @ColumnInfo(name="poster_path") val poster_path:String?,
    @ColumnInfo(name="original_language") val original_language: String?,
    @ColumnInfo(name="original_title") val original_title:String?,
    @ColumnInfo(name="original_name") val original_name:String?,
    @ColumnInfo(name="backdrop_path") val backdrop_path: String?,
    @ColumnInfo(name="adult") val adult: Boolean?,
    @ColumnInfo(name="overview") val overview: String?,
    @ColumnInfo(name="release_date") val release_date: String?,
    @ColumnInfo(name="first_air_date") val first_air_date: String?,
    @ColumnInfo(name="type") val type: Int
)

fun Movie.toDataBase(type: Int) = MovieEntity(
    vote_count=vote_count,
    id=id,
    video=video,
    vote_average=vote_average,
    title=title,
    name=name,
    popularity=popularity,
    poster_path=poster_path,
    original_language=original_language,
    original_title=original_title,
    original_name=original_name,
    backdrop_path=backdrop_path,
    adult=adult,
    overview=overview,
    release_date=release_date,
    first_air_date=first_air_date,
    type=type
)