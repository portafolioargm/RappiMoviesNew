package com.argm.rappimoviesnew.data.network

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.model.MoviesModel
import com.argm.rappimoviesnew.data.model.TVsModel
import com.argm.rappimoviesnew.data.model.TvModel
import retrofit2.Response
import retrofit2.http.*

interface TvClient {
    @GET(Constants.URL_TV_POPULAR)
    suspend fun getTVsPopular(): Response<TVsModel>
    @GET(Constants.URL_TV_TOP_RATED)
    suspend fun getTVsTopRated(): Response<TVsModel>
}