package com.argm.rappimoviesnew.data.database.dao

import androidx.room.*
import com.argm.rappimoviesnew.data.database.entities.MovieEntity
import com.argm.rappimoviesnew.data.database.entities.TvEntity

@Dao
interface TvDao {

    @Query("SELECT * FROM tv_table WHERE type=:type")
    suspend fun getAllTvsDB(type: Int): List<TvEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertTvsOnlyTypeDB(tvs: List<TvEntity>)

    @Delete
    suspend fun deleteTvsOnlyTypeDB(tvs: List<TvEntity>)

    @Query("DELETE FROM tv_table WHERE type=:type")
    suspend fun deleteMoviesForTypeDB(type: Int)
}