package com.argm.rappimoviesnew.data.repository

import com.argm.rappimoviesnew.data.database.dao.VideoDao
import com.argm.rappimoviesnew.data.database.entities.VideoEntity
import com.argm.rappimoviesnew.data.network.ApiService
import com.argm.rappimoviesnew.domain.model.Video
import com.argm.rappimoviesnew.domain.model.toDomain
import retrofit2.http.Url
import javax.inject.Inject

class VideoRepository  @Inject constructor(
    private val api : ApiService,
    private val dao : VideoDao
    ){
    suspend fun getVideosForIdApi(@Url url: String?, idMovie: Int): List<Video> {
        return try {
            (api.getVideosForID(url)).results.map { it.toDomain(idMovie) }
        }catch (e: Exception){
            emptyList()
        }
    }

    suspend fun getVideosForIdDB(idMovie: Int): List<Video> {
        return try {
            (dao.getAllVideosDB(idMovie)).map { it.toDomain(idMovie) }
        }catch (e: Exception){
            emptyList()
        }
    }

    suspend fun insertVideosDB(videos: List<VideoEntity>){
        dao.insertVideosOnlyTypeDB(videos)
    }

    suspend fun removeVideosDB(idMovie: Int){
        dao.deleteVideosMovieDB(idMovie)
    }
}