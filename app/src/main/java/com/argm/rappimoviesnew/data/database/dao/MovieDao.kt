package com.argm.rappimoviesnew.data.database.dao

import androidx.room.*
import com.argm.rappimoviesnew.data.database.entities.MovieEntity

@Dao
interface MovieDao {

    @Query("SELECT * FROM movie_table WHERE type=:type")
    suspend fun getAllMoviesDB(type: Int): List<MovieEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMoviesOnlyTypeDB(movies: List<MovieEntity>)

    @Delete
    suspend fun deleteMoviesOnlyTypeDB(movies: List<MovieEntity>)

    @Query("DELETE FROM movie_table WHERE type=:type")
    suspend fun deleteMoviesForTypeDB(type: Int)
}