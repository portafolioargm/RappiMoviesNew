package com.argm.rappimoviesnew.data.network

import android.util.Log
import com.argm.rappimoviesnew.data.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.http.Url
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

//Ojo por si hay que quitar @Singleton
@Singleton
class ApiService @Inject constructor(private val apiVideo: VideoClient, private val apiMovie : MovieClient, private  val apiTv : TvClient){
    /**
     * Obtener las peliculas mas populares
     */
    suspend fun getMoviesPopular(): MoviesModel {
        return withContext(Dispatchers.IO){
            try{
                val response = apiMovie.getMoviesPopular()
                (response.body() ?: emptyList<MovieModel>()) as MoviesModel
            }catch (e:Exception){
                emptyList<MovieModel>() as MoviesModel
            }
        }
    }
    /**
     * Obtener las peliculas en tendencia
     */
    suspend fun getMoviesTopRated(): MoviesModel {
        return withContext(Dispatchers.IO){
            try{
                val response = apiMovie.getMoviesTopRated()
                (response.body() ?: emptyList<MovieModel>()) as MoviesModel
            }catch (e:Exception){
                emptyList<MovieModel>() as MoviesModel
            }
        }

    }
    /**
     * Obtener las series mas populares
     */
    suspend fun getTVsPopular(): TVsModel {
        return withContext(Dispatchers.IO){
            try{
                val response = apiTv.getTVsPopular()
                (response.body() ?: emptyList<TvModel>()) as TVsModel
            }catch (e:Exception){
                emptyList<TvModel>() as TVsModel
            }
        }
    }
    /**
     * Obtener las series en tendencia
     */
    suspend fun getTVsTopRated(): TVsModel {
        return withContext(Dispatchers.IO){
            try{
                val response = apiTv.getTVsTopRated()
                (response.body() ?: emptyList<TvModel>()) as TVsModel
            }catch (e:Exception){
                emptyList<TvModel>() as TVsModel
            }
        }
    }

    /**
     * Obtener los videos que pertenecen a un ID
     */
    suspend fun getVideosForID(@Url url: String?): VideosModel {
        return withContext(Dispatchers.IO){
            try{
                val response = apiVideo.getVideosForId(url)
                (response.body() ?: emptyList<VideoModel>()) as VideosModel
            }catch (e:Exception){
                emptyList<VideoModel>() as VideosModel
            }
        }
    }
}