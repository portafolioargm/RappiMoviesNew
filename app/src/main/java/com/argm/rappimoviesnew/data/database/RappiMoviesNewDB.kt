package com.argm.rappimoviesnew.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.database.dao.MovieDao
import com.argm.rappimoviesnew.data.database.dao.TvDao
import com.argm.rappimoviesnew.data.database.dao.VideoDao
import com.argm.rappimoviesnew.data.database.entities.MovieEntity
import com.argm.rappimoviesnew.data.database.entities.TvEntity
import com.argm.rappimoviesnew.data.database.entities.VideoEntity

@Database(entities = [MovieEntity::class, TvEntity::class, VideoEntity::class], version = Constants.VERSION_SQLITE)
abstract class RappiMoviesNewDB: RoomDatabase() {
    abstract fun getMoviesDao(): MovieDao
    abstract fun getTvsDao(): TvDao
    abstract fun getVideosDao(): VideoDao
}