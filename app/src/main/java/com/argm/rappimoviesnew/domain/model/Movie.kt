package com.argm.rappimoviesnew.domain.model;

import android.graphics.Bitmap
import android.os.Parcelable
import com.argm.rappimoviesnew.data.database.entities.MovieEntity
import com.argm.rappimoviesnew.data.model.MovieModel
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie(
        val vote_count: Int?,
        val id: Int?,
        val video: Boolean?,
        val vote_average: Float?,
        val title:String?,
        val name: String?,
        val popularity:Float?,
        val poster_path:String?,
        val original_language: String?,
        val original_title:String?,
        val original_name:String?,
        val backdrop_path: String?,
        val adult: Boolean?,
        val overview: String?,
        val release_date: String?,
        val first_air_date: String?,
        val type: Int?
) : Parcelable

fun MovieModel.toDomain(type: Int) = Movie(
        vote_count,
        id,
        video,
        vote_average,
        title,
        name,
        popularity,
        poster_path,
        original_language,
        original_title,
        original_name,
        backdrop_path,
        adult,
        overview,
        release_date,
        first_air_date,
        type
)

fun MovieEntity.toDomain(type: Int) = Movie(
        vote_count,
        id,
        video,
        vote_average,
        title,
        name,
        popularity,
        poster_path,
        original_language,
        original_title,
        original_name,
        backdrop_path,
        adult,
        overview,
        release_date,
        first_air_date,
        type
)
