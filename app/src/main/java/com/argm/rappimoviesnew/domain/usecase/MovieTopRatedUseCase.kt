package com.argm.rappimoviesnew.domain.usecase;

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.repository.MovieRepository
import com.argm.rappimoviesnew.data.database.entities.toDataBase
import com.argm.rappimoviesnew.domain.model.Movie
import javax.inject.Inject

class MovieTopRatedUseCase @Inject constructor(private val repository : MovieRepository) {
    suspend operator fun invoke(): List<Movie> {
        val movies = repository.getMoviesFromApi(Constants.TYPE_MOVIES_TOP_RATED)
        return if (movies.isNotEmpty()){
            repository.removeForTypeMoviesDB(Constants.TYPE_MOVIES_TOP_RATED)
            repository.insertMoviesDB(movies.map { it.toDataBase(Constants.TYPE_MOVIES_TOP_RATED) })
            movies
        }else{
            repository.getMoviesFromDB(Constants.TYPE_MOVIES_TOP_RATED)
        }
    }
}
