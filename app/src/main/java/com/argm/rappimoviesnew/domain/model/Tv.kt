package com.argm.rappimoviesnew.domain.model;

import android.graphics.Bitmap
import android.os.Parcelable
import com.argm.rappimoviesnew.data.database.entities.MovieEntity
import com.argm.rappimoviesnew.data.database.entities.TvEntity
import com.argm.rappimoviesnew.data.model.MovieModel
import com.argm.rappimoviesnew.data.model.TvModel
import kotlinx.parcelize.Parcelize

@Parcelize
data class Tv(
        val poster_path: String?,
        val popularity:Float?,
        val id: Int?,
        val backdrop_path: String?,
        val vote_average:Float?,
        val overview:String?,
        val first_air_date:String?,
        val original_language:String?,
        val vote_count:Int?,
        val name: String?,
        val original_name:String?,
        val type: Int?
) : Parcelable

fun TvModel.toDomain(type: Int) = Tv(
        poster_path,
        popularity,
        id,
        backdrop_path,
        vote_average,
        overview,
        first_air_date,
        original_language,
        vote_count,
        name,
        original_name,
        type
)

fun TvEntity.toDomain(type: Int) = Tv(
        poster_path,
        popularity,
        id,
        backdrop_path,
        vote_average,
        overview,
        first_air_date,
        original_language,
        vote_count,
        name,
        original_name,
        type
)
