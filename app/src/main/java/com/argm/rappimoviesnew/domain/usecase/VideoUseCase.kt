package com.argm.rappimoviesnew.domain.usecase;

import com.argm.rappimoviesnew.data.repository.VideoRepository
import com.argm.rappimoviesnew.data.database.entities.toDataBase
import com.argm.rappimoviesnew.domain.model.Video
import retrofit2.http.Url
import javax.inject.Inject

class VideoUseCase @Inject constructor(private val repository : VideoRepository) {
    suspend operator fun invoke(@Url url:String, idMovie: Int): List<Video> {
        val videos = repository.getVideosForIdApi(url, idMovie)
        return if (videos.isNotEmpty()){
            repository.removeVideosDB(idMovie)
            repository.insertVideosDB(videos.map { it.toDataBase(idMovie) })
            videos
        }else{
            repository.getVideosForIdDB(idMovie)
        }
    }
}
