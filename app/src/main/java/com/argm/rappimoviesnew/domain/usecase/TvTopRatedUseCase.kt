package com.argm.rappimoviesnew.domain.usecase;

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.repository.TvRepository
import com.argm.rappimoviesnew.data.database.entities.toDataBase
import com.argm.rappimoviesnew.domain.model.Tv
import javax.inject.Inject

class TvTopRatedUseCase @Inject constructor(private val repository : TvRepository) {
    suspend operator fun invoke(): List<Tv> {
        val tvs = repository.getTVsFromApi(Constants.TYPE_MOVIES_TOP_RATED)
        return if (tvs.isNotEmpty()){
            repository.removeForTypeTvsDB(Constants.TYPE_MOVIES_TOP_RATED)
            repository.insertTvsDB(tvs.map { it.toDataBase(Constants.TYPE_MOVIES_TOP_RATED) })
            tvs
        }else{
            repository.getTvsFromDB(Constants.TYPE_MOVIES_TOP_RATED)
        }
    }
}
