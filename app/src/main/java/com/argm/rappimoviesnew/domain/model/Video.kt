package com.argm.rappimoviesnew.domain.model;

import android.os.Parcelable
import com.argm.rappimoviesnew.data.database.entities.VideoEntity
import com.argm.rappimoviesnew.data.model.VideoModel
import kotlinx.parcelize.Parcelize

@Parcelize
data class Video(
        val iso_639_1:String?,
        val iso_3166_1: String?,
        val name:String?,
        val key:String?,
        val site:String?,
        val size:String?,
        val type:String?,
        val official:Boolean?,
        val published_at:String?,
        val id:String?,
        val id_movie: Int?
) : Parcelable

fun VideoModel.toDomain(idMovie: Int) = Video(
        iso_639_1,
        iso_3166_1,
        name,
        key,
        site,
        size,
        type,
        official,
        published_at,
        id,
        idMovie
)

fun VideoEntity.toDomain(idMovie: Int) = Video(
        iso_639_1,
        iso_3166_1,
        name,
        key,
        site,
        size,
        type,
        official,
        published_at,
        id,
        idMovie
)
