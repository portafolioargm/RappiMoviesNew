package com.argm.rappimoviesnew.domain.usecase;

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.repository.MovieRepository
import com.argm.rappimoviesnew.data.database.entities.toDataBase
import com.argm.rappimoviesnew.domain.model.Movie
import javax.inject.Inject

class MoviePopularUseCase @Inject constructor(private val repository : MovieRepository) {
    suspend operator fun invoke(): List<Movie> {
        //0 popular //2 topRate
        val movies = repository.getMoviesFromApi(Constants.TYPE_MOVIES_POPULAR)
        return if (movies.isNotEmpty()){
            repository.removeForTypeMoviesDB(Constants.TYPE_MOVIES_POPULAR)
            repository.insertMoviesDB(movies.map { it.toDataBase(Constants.TYPE_MOVIES_POPULAR) })
            movies
        }else{
            //0 popular //2 topRate
            repository.getMoviesFromDB(Constants.TYPE_MOVIES_POPULAR)
        }
    }
}
