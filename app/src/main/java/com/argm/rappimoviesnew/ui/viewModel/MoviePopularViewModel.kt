package com.argm.rappimoviesnew.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.argm.rappimoviesnew.domain.usecase.MoviePopularUseCase
import com.argm.rappimoviesnew.domain.model.Movie
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class MoviePopularViewModel @Inject constructor(
    private val moviePopularUseCase : MoviePopularUseCase
) : ViewModel() {
    //LiveData
    val moviesModel = MutableLiveData<List<Movie>>()
    val isLoading =  MutableLiveData<Boolean>()

    fun onCreate() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = moviePopularUseCase()
            if (!result.isNullOrEmpty()) {
                moviesModel.postValue(result)
            }
            isLoading.postValue(false)
        }
    }
}