package com.argm.rappimoviesnew.ui.view.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.argm.rappimoviesnew.R
import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.model.MovieModel
import com.argm.rappimoviesnew.data.model.MoviesModel
import com.argm.rappimoviesnew.databinding.ItemMovieBinding
import com.argm.rappimoviesnew.domain.model.Movie
import com.argm.rappimoviesnew.ui.view.activities.DetailActivity
import com.bumptech.glide.Glide
import java.util.*

class MovieAdapter(private val movies : List<Movie>): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(inflater.inflate(R.layout.item_movie, parent, false))
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = movies[position]
        holder.render(item)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    class MovieViewHolder(val view: View):RecyclerView.ViewHolder(view) {
        val binding = ItemMovieBinding.bind(view)

        fun render(movie: Movie){
            binding.rating.rating = movie.vote_average!! / 2
            binding.txtYear.text = movie.release_date
            binding.txtTitle.text = movie.title
            binding.txtLanguage.text = movie.original_language!!.uppercase(Locale.getDefault())
            Glide
                .with(binding.imgPoster.context)
                .load(Constants.URL_PATH_POSTER + movie.poster_path)
                .centerInside()
                .into(binding.imgPoster);
            binding.content.setOnClickListener(View.OnClickListener {
                val detail = Intent(binding.content.context, DetailActivity::class.java)
                detail.putExtra("movie", movie)
                binding.content.context.startActivity(detail)
            })
        }
    }
}