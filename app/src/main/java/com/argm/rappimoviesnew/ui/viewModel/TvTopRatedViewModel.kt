package com.argm.rappimoviesnew.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.argm.rappimoviesnew.domain.usecase.TvTopRatedUseCase
import com.argm.rappimoviesnew.domain.model.Tv
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class TvTopRatedViewModel @Inject constructor(
    private val tvTopRatedUseCase : TvTopRatedUseCase
) : ViewModel() {
    //LiveData
    val tvsModel = MutableLiveData<List<Tv>>()
    val isLoading =  MutableLiveData<Boolean>()

    fun onCreate() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = tvTopRatedUseCase()
            if (!result.isNullOrEmpty()) {
                tvsModel.postValue(result)
            }
            isLoading.postValue(false)
        }
    }
}