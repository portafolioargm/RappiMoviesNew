package com.argm.rappimoviesnew.ui.view.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.argm.rappimoviesnew.R
import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.model.MovieModel
import com.argm.rappimoviesnew.data.model.MoviesModel
import com.argm.rappimoviesnew.data.model.TVsModel
import com.argm.rappimoviesnew.data.model.TvModel
import com.argm.rappimoviesnew.databinding.ItemMovieBinding
import com.argm.rappimoviesnew.domain.model.Tv
import com.argm.rappimoviesnew.ui.view.activities.DetailActivity
import com.bumptech.glide.Glide
import java.util.*

class TvAdapter(private val tvs : List<Tv>): RecyclerView.Adapter<TvAdapter.TvViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TvViewHolder(inflater.inflate(R.layout.item_movie, parent, false))
    }

    override fun onBindViewHolder(holder: TvViewHolder, position: Int) {
        val item = tvs[position]
        holder.render(item)
    }

    override fun getItemCount(): Int {
        return tvs.size
    }

    class TvViewHolder(val view: View):RecyclerView.ViewHolder(view) {
        val binding = ItemMovieBinding.bind(view)

        fun render(tv: Tv){
            binding.rating.rating = (tv.vote_average!! / 2).toFloat()
            binding.txtYear.text = tv.first_air_date
            binding.txtTitle.text = tv.name
            binding.txtLanguage.text = tv.original_language!!.uppercase(Locale.getDefault())
            Glide
                .with(binding.imgPoster.context)
                .load(Constants.URL_PATH_POSTER + tv.poster_path)
                .centerInside()
                .into(binding.imgPoster);
            binding.content.setOnClickListener(View.OnClickListener {
                val detail = Intent(binding.content.context, DetailActivity::class.java)
                detail.putExtra("tv", tv)
                binding.content.context.startActivity(detail)
            })
        }
    }
}

