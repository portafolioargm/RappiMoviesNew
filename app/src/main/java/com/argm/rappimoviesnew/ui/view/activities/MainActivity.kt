package com.argm.rappimoviesnew.ui.view.activities

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.argm.rappimoviesnew.R
import com.argm.rappimoviesnew.databinding.ActivityMainBinding
import com.argm.rappimoviesnew.ui.view.adapters.MovieAdapter
import com.argm.rappimoviesnew.ui.view.adapters.TvAdapter
import com.argm.rappimoviesnew.ui.viewModel.MoviePopularViewModel
import com.argm.rappimoviesnew.ui.viewModel.MovieTopRatedViewModel
import com.argm.rappimoviesnew.ui.viewModel.TvPopularViewModel
import com.argm.rappimoviesnew.ui.viewModel.TvTopRatedViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import dagger.hilt.android.AndroidEntryPoint

@Suppress("DEPRECATION")
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val moviePopularViewModel: MoviePopularViewModel by viewModels()
    private val tvPopularViewModel: TvPopularViewModel by viewModels()
    private val movieTopRatedViewModel: MovieTopRatedViewModel by viewModels()
    private val tvTopRatedViewModel: TvTopRatedViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.rvMovies.layoutManager = GridLayoutManager(applicationContext,2)

        binding.navView.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_popular -> {
                    if (binding.tabs.selectedTabPosition==0){
                        moviePopularViewModel.onCreate()
                    }else{
                        tvPopularViewModel.onCreate()
                    }
                    true
                }
                R.id.action_top_rated -> {
                    if (binding.tabs.selectedTabPosition==0){
                        movieTopRatedViewModel.onCreate()
                    }else{
                        tvTopRatedViewModel.onCreate()
                    }
                    true
                }
                else -> {
                    if (binding.tabs.selectedTabPosition==0){
                        moviePopularViewModel.onCreate()
                    }else{
                        tvPopularViewModel.onCreate()
                    }
                    true
                }
            }
        }
        binding.navView.selectedItemId = R.id.action_popular
        binding.tabs.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                binding.navView.selectedItemId = R.id.action_popular
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        moviePopularViewModel.moviesModel.observe(this, Observer {
            binding.rvMovies.adapter = MovieAdapter(moviePopularViewModel.moviesModel.value!!)
        })
        moviePopularViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
            binding.rvMovies.isVisible = !it
        })

        movieTopRatedViewModel.moviesModel.observe(this, Observer {
            binding.rvMovies.adapter = MovieAdapter(movieTopRatedViewModel.moviesModel.value!!)
        })
        movieTopRatedViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
            binding.rvMovies.isVisible = !it
        })

        tvPopularViewModel.tvsModel.observe(this, Observer {
            binding.rvMovies.adapter = TvAdapter(tvPopularViewModel.tvsModel.value!!)
        })
        tvPopularViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
            binding.rvMovies.isVisible = !it
        })

        tvTopRatedViewModel.tvsModel.observe(this, Observer {
            binding.rvMovies.adapter = TvAdapter(tvTopRatedViewModel.tvsModel.value!!)
        })
        movieTopRatedViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
            binding.rvMovies.isVisible = !it
        })
    }
}