package com.argm.rappimoviesnew.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.argm.rappimoviesnew.domain.usecase.MovieTopRatedUseCase
import com.argm.rappimoviesnew.domain.model.Movie
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class MovieTopRatedViewModel @Inject constructor(
    private val movieTopRatedUseCase : MovieTopRatedUseCase
) : ViewModel() {
    //LiveData
    val moviesModel = MutableLiveData<List<Movie>>()
    val isLoading =  MutableLiveData<Boolean>()

    fun onCreate() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = movieTopRatedUseCase()
            if (!result.isNullOrEmpty()) {
                moviesModel.postValue(result)
            }
            isLoading.postValue(false)
        }
    }
}