package com.argm.rappimoviesnew.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.argm.rappimoviesnew.domain.usecase.VideoUseCase
import com.argm.rappimoviesnew.domain.model.Video
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class VideoViewModel @Inject constructor(
    private val videoUseCase : VideoUseCase
) : ViewModel() {
    //LiveData
    val videosModel = MutableLiveData<List<Video>>()
    val isLoading =  MutableLiveData<Boolean>()

    fun onCreate(url:String, idMovie: Int) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = videoUseCase(url, idMovie)
            if (!result.isNullOrEmpty()) {
                videosModel.postValue(result)
            }
            isLoading.postValue(false)
        }
    }
}