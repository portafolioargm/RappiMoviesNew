package com.argm.rappimoviesnew.ui.view.adapters

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.argm.rappimoviesnew.R
import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.model.*
import com.argm.rappimoviesnew.databinding.ItemMovieBinding
import com.argm.rappimoviesnew.databinding.ItemVideoBinding
import com.argm.rappimoviesnew.domain.model.Video
import com.argm.rappimoviesnew.ui.view.activities.DetailActivity
import com.bumptech.glide.Glide

class VideoAdapter(private val videos : List<Video>): RecyclerView.Adapter<VideoAdapter.TvViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TvViewHolder(inflater.inflate(R.layout.item_video, parent, false))
    }

    override fun onBindViewHolder(holder: TvViewHolder, position: Int) {
        val item = videos[position]
        holder.render(item)
    }

    override fun getItemCount(): Int {
        return videos.size
    }

    class TvViewHolder(val view: View):RecyclerView.ViewHolder(view) {
        val binding = ItemVideoBinding.bind(view)
        fun render(video: Video){
            if (video.site?.uppercase().equals("YOUTUBE")){
                binding.imItemReproducir.visibility = View.VISIBLE
                binding.imItemReproducir.setOnClickListener(View.OnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(Constants.URL_YOUTUBE + video.key)
                    binding.content.context.startActivity(intent)
                })
            }else{
                binding.imItemReproducir.visibility = View.GONE
            }
            binding.txtItemLenguaje.text = video.iso_639_1
            binding.txtItemTitulo.text = video.name
            binding.txtItemTipo.text = video.type
            binding.txtItemReproductor.text = video.site
        }
    }
}

