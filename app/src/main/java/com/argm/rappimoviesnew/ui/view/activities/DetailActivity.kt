package com.argm.rappimoviesnew.ui.view.activities

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.model.MovieModel
import com.argm.rappimoviesnew.data.model.TvModel
import com.argm.rappimoviesnew.databinding.ActivityDetailBinding
import com.argm.rappimoviesnew.domain.model.Movie
import com.argm.rappimoviesnew.domain.model.Tv
import com.argm.rappimoviesnew.ui.view.adapters.VideoAdapter
import com.argm.rappimoviesnew.ui.viewModel.TvTopRatedViewModel
import com.argm.rappimoviesnew.ui.viewModel.VideoViewModel
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailActivity() : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    private val videoViewModel: VideoViewModel by viewModels()
    private var movie : Movie? = null
    private var tv : Tv? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = intent.extras
        try {
            movie = bundle?.getParcelable<Movie>("movie")!!
        }catch (e: Exception){
            tv = bundle?.getParcelable<Tv>("tv")!!
        }
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.rvVideos.layoutManager = LinearLayoutManager(this)
        
        if (tv != null){
            binding.encabezado.txItemTitulo.text = tv?.name
            binding.encabezado.txItemFechaLanzamiento.text = tv?.first_air_date
            Glide
                .with(this)
                .load(Constants.URL_PATH_POSTER + tv?.poster_path)
                .centerInside()
                .into(binding.encabezado.imgPoster);
            binding.txItemTeamDescription.text = tv?.overview
            binding.encabezado.rating.rating = tv?.vote_average!! / 2
            val url = Constants.URL_TV_FOR_ID.replace("{id_tv}", tv?.id.toString(), false)
            videoViewModel.onCreate(url, tv?.id!!)
        }else{
            binding.encabezado.txItemTitulo.text = movie?.title
            binding.encabezado.txItemFechaLanzamiento.text = movie?.release_date
            Glide
                .with(this)
                .load(Constants.URL_PATH_POSTER + movie?.poster_path)
                .centerInside()
                .into(binding.encabezado.imgPoster);
            binding.txItemTeamDescription.text = movie?.overview
            binding.encabezado.rating.rating = movie?.vote_average!! / 2
            val url = Constants.URL_VIDEOS_FOR_ID.replace("{id_movie}", movie?.id.toString(), false)
            videoViewModel.onCreate(url, movie?.id!!)
        }
        videoViewModel.videosModel.observe(this, Observer {
            binding.rvVideos.adapter = VideoAdapter(videoViewModel.videosModel.value!!)
        })
        videoViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
            binding.rvVideos.isVisible = !it
        })

    }
}