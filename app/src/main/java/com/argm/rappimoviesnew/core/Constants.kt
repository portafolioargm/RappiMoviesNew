package com.argm.rappimoviesnew.core
object Constants {
    //DOMINIO
    const val URL_API = "https://api.themoviedb.org/3/"

    //DB
    const val VERSION_SQLITE = 1
    const val NAME_DB = "rappi_movies_new"
    const val TABLE_DB_MOVIE = "movie_table"
    const val TABLE_DB_TV = "tv_table"
    const val TABLE_DB_VIDEO = "video_table"

    //API
    const val API_KEY = "37a97ca4fc0278ad1e04fc3b857f1dec"
    const val API_KEY_YOUTUBE = "AIzaSyCCM4_3c2bsVbWndY0UeE0kZVxWprSIEIA"
    const val API_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzN2E5N2NhNGZjMDI3OGFkMWUwNGZjM2I4NTdmMWRlYyIsInN1YiI6IjViODU4Mzc4OTI1MTQxNTlkNzA1M2RiYSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Yeivtod1c0TpeKWM0__ANASRa_ewhR9VLKFgqVl7zOY"

    //URLs
    const val URL_MOVIES_POPULAR = URL_API + "movie/popular?api_key=$API_KEY"
    const val URL_MOVIES_TOP_RATED = URL_API + "movie/top_rated?api_key=$API_KEY"
    const val URL_MOVIES_UPCOMING = URL_API + "movie/upcoming?api_key=$API_KEY"
    const val URL_TV_POPULAR = URL_API + "tv/popular?api_key=$API_KEY"
    const val URL_TV_TOP_RATED = URL_API + "tv/top_rated?api_key=$API_KEY"
    const val URL_VIDEOS_FOR_ID = URL_API + "movie/{id_movie}/videos?api_key=$API_KEY"
    const val URL_TV_FOR_ID = URL_API + "tv/{id_tv}/videos?api_key=$API_KEY"
    const val URL_PATH_POSTER = "https://image.tmdb.org/t/p/w600_and_h900_bestv2/"
    const val URL_YOUTUBE = "https://www.youtube.com/watch?v="

    //COMUN
    const val TYPE_MOVIES_POPULAR = 0
    const val TYPE_MOVIES_TOP_RATED = 2
}