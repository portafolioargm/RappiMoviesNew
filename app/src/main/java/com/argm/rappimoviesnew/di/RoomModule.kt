package com.argm.rappimoviesnew.di

import android.content.Context
import androidx.room.Room
import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.database.RappiMoviesNewDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, RappiMoviesNewDB::class.java, Constants.NAME_DB)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()

    @Singleton
    @Provides
    fun provideMovieDao(db: RappiMoviesNewDB) = db.getMoviesDao()

    @Singleton
    @Provides
    fun provideTvDao(db: RappiMoviesNewDB) = db.getTvsDao()

    @Singleton
    @Provides
    fun provideVideoDao(db: RappiMoviesNewDB) = db.getVideosDao()
}