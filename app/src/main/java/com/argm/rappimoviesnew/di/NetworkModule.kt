package com.argm.rappimoviesnew.di

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.network.MovieClient
import com.argm.rappimoviesnew.data.network.TvClient
import com.argm.rappimoviesnew.data.network.VideoClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    //@Singleton Mantiene un patron de una unica instancia
     @Singleton
     @Provides
     fun provideRetrofit(): Retrofit{
         return Retrofit.Builder()
             .baseUrl(Constants.URL_API)
             .addConverterFactory(GsonConverterFactory.create())
             .build()
     }

    @Singleton
    @Provides
    fun provideMovieClient(retrofit: Retrofit): MovieClient {
        return retrofit.create(MovieClient::class.java)
    }

    @Singleton
    @Provides
    fun provideTvClient(retrofit: Retrofit): TvClient {
        return retrofit.create(TvClient::class.java)
    }

    @Singleton
    @Provides
    fun provideVideoClient(retrofit: Retrofit): VideoClient {
        return retrofit.create(VideoClient::class.java)
    }
}