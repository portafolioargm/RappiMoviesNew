package com.argm.rappimoviesnew

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RappiMoviesApp: Application() {
}