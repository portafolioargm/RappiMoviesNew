package com.argm.rappimoviesnew.domain

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.repository.MovieRepository
import com.argm.rappimoviesnew.domain.model.Movie
import com.argm.rappimoviesnew.domain.usecase.MovieTopRatedUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class MovieTopRatedUseCaseTest{
    @RelaxedMockK
    private lateinit var repository : MovieRepository

    lateinit var movieTopRatedUseCase: MovieTopRatedUseCase
    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        movieTopRatedUseCase = MovieTopRatedUseCase(repository)
    }

    @Test
    fun `cuando el api no retorna nada devuelve los valores de la DB para Movies Top Rate`() = runBlocking {
        //Given
        coEvery { repository.getMoviesFromApi(Constants.TYPE_MOVIES_TOP_RATED) } returns emptyList()
        //When
        movieTopRatedUseCase()
        //Then
        coVerify(exactly = 1) { repository.getMoviesFromDB(Constants.TYPE_MOVIES_TOP_RATED) }
        coVerify(exactly = 0) { repository.removeMoviesDB(any()) }
        coVerify(exactly = 0) { repository.insertMoviesDB(any()) }
    }

    @Test
    fun `cuando el api retorna un listado de movies Top Rated`() = runBlocking {
        val myList = listOf(Movie(10, 1, false, 10.0F, "Prueba Movie", "Pelicula", 5.0F, "prueba_poster", "ES", "Prueba Original Movie", "Pelicula Original", null, false, "Descripcion de la pelicula en general", "10-12-2021", "24-12-2021", Constants.TYPE_MOVIES_TOP_RATED))
        //Given
        coEvery { repository.getMoviesFromApi(Constants.TYPE_MOVIES_TOP_RATED) } returns myList
        //When
        val response = movieTopRatedUseCase()
        //Then
        coVerify(exactly = 1) { repository.removeMoviesDB(any()) }
        coVerify(exactly = 1) { repository.insertMoviesDB(any()) }
        coVerify(exactly = 0) { repository.getMoviesFromDB(Constants.TYPE_MOVIES_TOP_RATED) }
        assert(myList==response)
    }
}