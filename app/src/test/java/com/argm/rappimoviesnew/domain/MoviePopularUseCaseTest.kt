package com.argm.rappimoviesnew.domain

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.repository.MovieRepository
import com.argm.rappimoviesnew.domain.model.Movie
import com.argm.rappimoviesnew.domain.usecase.MoviePopularUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class MoviePopularUseCaseTest{
    @RelaxedMockK
    private lateinit var repository : MovieRepository

    lateinit var moviePopularUseCase: MoviePopularUseCase
    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        moviePopularUseCase = MoviePopularUseCase(repository)
    }

    @Test
    fun `cuando el api no retorna nada devuelve los valores de la DB para Movies Populares`() = runBlocking {
        //Given
        coEvery { repository.getMoviesFromApi(Constants.TYPE_MOVIES_POPULAR) } returns emptyList()
        //When
        moviePopularUseCase()
        //Then
        coVerify(exactly = 1) { repository.getMoviesFromDB(Constants.TYPE_MOVIES_POPULAR) }
    }

    @Test
    fun `cuando el api retorna un listado de movies Populares`() = runBlocking {
        val myList = listOf(Movie(10, 1, false, 10.0F, "Prueba Movie", "Pelicula", 5.0F, "prueba_poster", "ES", "Prueba Original Movie", "Pelicula Original", null, false, "Descripcion de la pelicula en general", "10-12-2021", "24-12-2021", Constants.TYPE_MOVIES_POPULAR))
        //Given
        coEvery { repository.getMoviesFromApi(Constants.TYPE_MOVIES_POPULAR) } returns myList
        //When
        val response = moviePopularUseCase()
        //Then
        coVerify(exactly = 1) { repository.removeMoviesDB(any()) }
        coVerify(exactly = 1) { repository.insertMoviesDB(any()) }
        coVerify(exactly = 0) { repository.getMoviesFromDB(Constants.TYPE_MOVIES_POPULAR) }
        assert(myList==response)
    }
}