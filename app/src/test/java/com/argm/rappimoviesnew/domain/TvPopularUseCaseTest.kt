package com.argm.rappimoviesnew.domain

import com.argm.rappimoviesnew.core.Constants
import com.argm.rappimoviesnew.data.repository.TvRepository
import com.argm.rappimoviesnew.domain.model.Tv
import com.argm.rappimoviesnew.domain.usecase.TvPopularUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class TvPopularUseCaseTest{
    @RelaxedMockK
    private lateinit var repository : TvRepository
    lateinit var tvPopularUseCase: TvPopularUseCase

    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        tvPopularUseCase = TvPopularUseCase(repository)
    }

    @Test
    fun `cuando el api no retorna nada devuelve los valores de la DB para Series Populares`() = runBlocking {
        //Given
        coEvery { repository.getTVsFromApi(Constants.TYPE_MOVIES_POPULAR) } returns emptyList()
        //When
        tvPopularUseCase()
        //Then
        coVerify(exactly = 1) { repository.getTvsFromDB(Constants.TYPE_MOVIES_POPULAR) }
    }

    @Test
    fun `cuando el api retorna un listado de series Populares`() = runBlocking {
        val myList = listOf(Tv("prueba poster", 1.0F, 1, "", 10.0F, "Descripcion de la Pelicula", "12-12-2020", "es", 9, "Prueba Movie", "Pelicula Original", Constants.TYPE_MOVIES_POPULAR))
        //Given
        coEvery { repository.getTVsFromApi(Constants.TYPE_MOVIES_POPULAR) } returns myList
        //When
        val response = tvPopularUseCase()
        //Then
        coVerify(exactly = 1) { repository.removeTvsDB(any()) }
        coVerify(exactly = 1) { repository.insertTvsDB(any()) }
        coVerify(exactly = 0) { repository.getTvsFromDB(Constants.TYPE_MOVIES_POPULAR) }
        assert(myList==response)
    }
}