package com.argm.rappimoviesnew.ui.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.argm.rappimoviesnew.domain.usecase.VideoUseCase
import com.argm.rappimoviesnew.domain.model.Video
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class VideoViewModelTest{
    @RelaxedMockK
    private lateinit var usecase : VideoUseCase

    lateinit var videoViewModel: VideoViewModel

    @get:Rule
    var rule: InstantTaskExecutorRule =  InstantTaskExecutorRule()

    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        videoViewModel = VideoViewModel(usecase)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter(){
        Dispatchers.resetMain()
    }

    @Test
    fun `cuando el viewModel Video es creado por primera vez, obtiene todos los videos correspondiente a un idMovie o a un idTv`() = runTest {
        val url = "https://api.themoviedb.org/3/movie/634649/videos?api_key=37a97ca4fc0278ad1e04fc3b857f1dec"
        val id = 1
        val myList = listOf(
            Video("en", "US", "Video Trailer", "JfVOs4VSpmA", "Youtube", "1080", "Trailer", true, "2021-11-17T01:30:05.000Z", "61945b8a4da3d4002992d5a6", 1),
            Video("en", "US", "Video Trailer2", "JfVOs4VSpmJ", "Youtube", "1080", "Trailer", true, "2020-11-17T01:30:05.000Z", "61945b8a4da3d4002992d5a0", 1))
        //Given
        coEvery { usecase(url, id)} returns myList
        //When
        videoViewModel.onCreate(url, id)
        //Then
        assert(videoViewModel.videosModel.value==myList)
    }
}